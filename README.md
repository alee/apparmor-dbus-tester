# D-Bus Server for Apparmor testing

## Build

Run autotooling to create configure script:

```
$ autoreconf -i
```

Follow standard configuration and build:

```
$ ./configure
$ make
```
