#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <systemd/sd-bus.h>

char *app_name;

static void say(const char *message) {
	printf("%s:%s\n", app_name, message);
}

static int method_foo(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "Foo called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}

static int method_foo2(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "Foo2 called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}

static int method_accept(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "Accept called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}

static int method_aaa(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "AuditAndAccept called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}

static int method_deny(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "Deny called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}

static int method_aad(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	char *message;
        int r;
	char *ret = "AuditAndDeny called.";

        /* Read the parameters */
        r = sd_bus_message_read(m, "s", &message);
        if (r < 0) {
                fprintf(stderr, "Failed to parse parameters: %s\n", strerror(-r));
                return r;
        }

	say(ret);

        /* Reply with the response */
        return sd_bus_reply_method_return(m, "s", ret);
}


/* The vtable of our little object, implements the org.Test interface */
static const sd_bus_vtable test_vtable[] = {
        SD_BUS_VTABLE_START(0),
        SD_BUS_METHOD("Foo", "s", "s", method_foo, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_METHOD("Foo2", "s", "s", method_foo2, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_METHOD("Accept", "s", "s", method_accept, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_METHOD("AuditAndAccept", "s", "s", method_aaa, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_METHOD("Deny", "s", "s", method_deny, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_METHOD("AuditAndDeny", "s", "s", method_aad, SD_BUS_VTABLE_UNPRIVILEGED),
        SD_BUS_VTABLE_END
};

#define BUF_SIZE 10

int main(int argc, char *argv[]) {
        sd_bus_slot *slot = NULL;
        sd_bus *bus = NULL;
        int r;
	FILE *fp;
	char *rp;
	char buf[BUF_SIZE];

	app_name = argv[0];

        /* Connect to the user bus this time */
        r = sd_bus_open_user(&bus);
        if (r < 0) {
                fprintf(stderr, "Failed to connect to session bus: %s\n", strerror(-r));
                goto finish;
        }

	say("Starting");

        /* Install the object */
        r = sd_bus_add_object_vtable(bus,
                                     &slot,
                                     "/org/Test",  /* object path */
                                     "org.Test",   /* interface name */
                                     test_vtable,
                                     NULL);
        if (r < 0) {
                fprintf(stderr, "Failed to issue method call: %s\n", strerror(-r));
                goto finish;
        }

	printf("%s:AppArmor context: ", app_name);
	fp = fopen("/proc/self/attr/current", "r");
	if (!fp) {
                fprintf(stderr, "Failed to open AppArmor context: %s\n", strerror(errno));
                goto finish;
	}

	while( 1 ) {
		rp = fgets(buf, BUF_SIZE, fp);
		if (!r)
			break;

		puts(buf);
	}
	printf("\n");
	fclose(fp);

        say("service ready");

	say("Obtaining bus name");

        /* Take a well-known service name so that clients can find us */
        r = sd_bus_request_name(bus, "org.Test", 0);
        if (r < 0) {
                fprintf(stderr, "Failed to acquire service name: %s\n", strerror(-r));
                goto finish;
        }

	say("Entering main loop");

        for (;;) {
                /* Process requests */
                r = sd_bus_process(bus, NULL);
                if (r < 0) {
                        fprintf(stderr, "Failed to process bus: %s\n", strerror(-r));
                        goto finish;
                }
                if (r > 0) /* we processed a request, try to process another one, right-away */
                        continue;

                /* Wait for the next request to process */
                r = sd_bus_wait(bus, (uint64_t) -1);
                if (r < 0) {
                        fprintf(stderr, "Failed to wait on bus: %s\n", strerror(-r));
                        goto finish;
                }
        }

	say("Leaving main loop");

finish:
        sd_bus_slot_unref(slot);
        sd_bus_unref(bus);

        return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
